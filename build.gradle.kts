plugins {
    java
    `maven-publish`
    id("com.github.johnrengelman.shadow") version("7.1.1")
}

repositories {
    mavenLocal()

    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
    maven("https://oss.sonatype.org/content/groups/public/")
    maven("https://repo.unnamed.team/repository/unnamed-public/")
    maven("https://repo.maven.apache.org/maven2/")
}

dependencies {
    implementation("me.fixeddev:commandflow-bukkit:0.4.0")
    compileOnly("org.spigotmc:spigot-api:1.17.1-R0.1-SNAPSHOT")
}

group = "me.jonakls.bomberman"
version = "1.0.0"
description = "Bomberman minigame"
java.sourceCompatibility = JavaVersion.VERSION_11

publishing {
    publications.create<MavenPublication>("maven") {
        from(components["java"])
    }
}

tasks {

    processResources {
        expand(
            "name" to rootProject.name,
            "version" to version
        )
    }

}
